@extends('layouts/main')

@section('konten')
<div class="card">
    <div class="card-header">
            <div class="float-left">
                <h4>Biodata Mahasiswa</h4>
            </div>
    </div>
    <div class="card-body">
        <table class="table table-bordered">
            <tbody>
                    <tr >
                        <td>
                            <img src="assets/me.jpeg" class="rounded" width="200" alt="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-sm-1">
                                    <h6>Nama </h6>
                                </div>
                                <div class="col-sm-11">
                                    <h6>I Komang Marjaya Adi Surya</h6>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-sm-1">
                                    <h6>NIM </h6>
                                </div>
                                <div class="col-sm-11">
                                    <h6>1915051088</h6>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-sm-1">
                                    <h6>Kelas </h6>
                                </div>
                                <div class="col-sm-11">
                                    <h6>PTI 5C</h6>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="row">
                                <div class="col-sm-1">
                                    <h6>Gitlab </h6>
                                </div>
                                <div class="col-sm-11">
                                    <h6>
                                        <a href="">Link Gitlab Untuk Tugas Ini</a>
                                    </h6>
                                </div>
                            </div>
                        </td>
                    </tr>
            </tbody>
        </table>
    </div>
</div>
@endsection